package ru.nsu.fit.mazhuga.raspred.cmd.utils;

import org.apache.commons.cli.*;
import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.InsertionStrategy;

public class CommandOptions {

    private static final String FILE_OPTION_NAME = "file";
    private static final String FILE_ARG_NAME = "path";
    private static final String FILE_OPTION_DESC = "parse an archived file with OpenStreetMap data and log stats";

    private static final String DB_TEST_OPTION_NAME = "dbtest";
    private static final String DB_TEST_ARG_NAME = "strategy";
    private static final String DB_TEST_DESC_NAME = "load data into database and calculate efficiency of chosen " +
            "insertion method (\"statement\", \"prepared\", \"batch\")";

    private final CommandLine commandLine;

    public CommandOptions(String[] args) throws ParseException {

        final var options = new Options();
        options.addOption(
                Option.builder(FILE_OPTION_NAME)
                        .desc(FILE_OPTION_DESC)
                        .hasArg()
                        .argName(FILE_ARG_NAME)
                        .build());
        options.addOption(
                Option.builder(DB_TEST_OPTION_NAME)
                        .desc(DB_TEST_DESC_NAME)
                        .hasArg()
                        .argName(DB_TEST_ARG_NAME)
                        .build());

        final var commandParser = new DefaultParser();
        commandLine = commandParser.parse(options, args);
    }

    public boolean hasFileOption() {
        return commandLine.hasOption(FILE_OPTION_NAME);
    }

    public String getFilePath() {
        return commandLine.getOptionValue(FILE_OPTION_NAME);
    }

    public boolean hasDbTestOption() {
        return commandLine.hasOption(DB_TEST_OPTION_NAME);
    }

    public InsertionStrategy getDbTestStrategy() {
        return InsertionStrategy.fromText(commandLine.getOptionValue(DB_TEST_OPTION_NAME));
    }
}
