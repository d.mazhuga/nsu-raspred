package ru.nsu.fit.mazhuga.raspred.cmd;

import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.DbConnector;
import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.DbInitializer;
import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders.DbNodeLoaderFactory;
import ru.nsu.fit.mazhuga.raspred.cmd.parsing.NodeParser;
import ru.nsu.fit.mazhuga.raspred.cmd.parsing.NodeStatsService;
import ru.nsu.fit.mazhuga.raspred.cmd.utils.CommandOptions;
import ru.nsu.fit.mazhuga.raspred.cmd.utils.PropertiesHolder;

public class Main {

    public static void main(String[] args) throws Exception {

        var commandOptions = new CommandOptions(args);

        if (commandOptions.hasFileOption()) {

            final var pathString = commandOptions.getFilePath();

            if (commandOptions.hasDbTestOption()) {

                final var dbStrategy = commandOptions.getDbTestStrategy();

                var propertiesHolder = PropertiesHolder.fromFile("/db.properties");
                final var dbConnector = new DbConnector(
                        propertiesHolder.getDatabaseUrl(),
                        propertiesHolder.getDatabaseUser(),
                        propertiesHolder.getDatabasePassword());

                final var dbInitializer = new DbInitializer(dbConnector);
                dbInitializer.initDb(true);

                final var dbLoaderFactory = new DbNodeLoaderFactory();

                final var parser = new NodeParser();
                final var service = new NodeStatsService(parser, dbConnector, dbLoaderFactory);

                service.parseAndLoadToDb(pathString, dbStrategy, true);

            } else {
                final var parser = new NodeParser();
                final var service = new NodeStatsService(parser, null, null);

                service.parse(pathString, true);
            }
        }
    }
}
