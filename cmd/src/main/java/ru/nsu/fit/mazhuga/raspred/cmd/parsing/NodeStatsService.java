package ru.nsu.fit.mazhuga.raspred.cmd.parsing;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.DbConnector;
import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.InsertionStrategy;
import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders.DbNodeLoaderFactory;

import java.util.Comparator;

@Slf4j
@RequiredArgsConstructor
public class NodeStatsService {

    private static final String DEFAULT_FILE_PATH = "RU-NVS.osm.bz2";
    private static final int PRINT_ENTRIES_LIMIT = 10;

    private final NodeParser parser;
    private final DbConnector dbConnector;
    private final DbNodeLoaderFactory dbNodeLoaderFactory;

    public ParsingResult parse(String pathString, boolean logResult) throws Exception {
        return parseInternal(pathString, false, null, logResult);
    }

    public ParsingResult parseAndLoadToDb(String pathString, InsertionStrategy strategy,
                                          boolean logResult) throws Exception {
        return parseInternal(pathString, true, strategy, logResult);
    }

    private ParsingResult parseInternal(String pathString, boolean loadToDb,
                                        InsertionStrategy strategy, boolean logResult) throws Exception {
        if (pathString == null) {
            pathString = DEFAULT_FILE_PATH;
        }

        ParsingResult result;

        if (loadToDb) {

            log.info("Will save data to DB and measure time, using insertion method {}", strategy.name());

            try (var connection = dbConnector.connect();
                 var dbLoader = dbNodeLoaderFactory.create(strategy, connection)) {
                result = parser.parseCompressedAndLoadToDb(pathString, dbLoader);
            }
        } else {
            result = parser.parseCompressed(pathString);
        }

        if (logResult) {
            prettyLogStats(result);
        }

        return result;
    }

    private void prettyLogStats(ParsingResult result) {

        if (result.getDbInsertedCount() != null && result.getDbTimeSpentMs() != null) {

            log.info("DB insertions: {} entries\nTime spent: {} ms\nDB insertion speed: {} entries/ms",
                    result.getDbInsertedCount(), result.getDbTimeSpentMs(),
                    (double) result.getDbInsertedCount() / result.getDbTimeSpentMs());
        }

        log.info("=== Top {} USERS by edits ===", PRINT_ENTRIES_LIMIT);
        result.getUserDataList().stream()
                .sorted(Comparator.comparing(ParsingResult.UserData::getEditsCount).reversed())
                .limit(PRINT_ENTRIES_LIMIT)
                .forEachOrdered(it -> log.info("Name: {} Edits: {}", it.getName(), it.getEditsCount()));

        log.info("=== Top {} NODES by edits ===", PRINT_ENTRIES_LIMIT);
        result.getNodeDataList().stream()
                .sorted(Comparator.comparing(ParsingResult.NodeData::getEditsCount).reversed())
                .limit(PRINT_ENTRIES_LIMIT)
                .forEachOrdered(it -> log.info("Name: {} Edits: {}", it.getName(), it.getEditsCount()));
    }
}
