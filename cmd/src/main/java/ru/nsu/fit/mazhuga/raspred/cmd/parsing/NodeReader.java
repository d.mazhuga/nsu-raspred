package ru.nsu.fit.mazhuga.raspred.cmd.parsing;

import ru.nsu.fit.mazhuga.raspred.cmd.generated.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import java.io.InputStream;

public class NodeReader implements AutoCloseable {

    private static final String NODE_ELEMENT_NAME = "node";

    private final XMLEventReader eventReader;

    private final Unmarshaller nodeUnmarshaller;

    public NodeReader(InputStream inputStream) throws XMLStreamException, JAXBException {
        eventReader = XMLInputFactory.newInstance().createXMLEventReader(inputStream);
        nodeUnmarshaller = JAXBContext.newInstance(Node.class).createUnmarshaller();
    }

    public Node nextNode() throws XMLStreamException, JAXBException {

        while (eventReader.hasNext()) {
            var nextEvent = eventReader.peek();

            if (nextEvent.isStartElement()) {
                StartElement element = nextEvent.asStartElement();

                if (element.getName().getLocalPart().equals(NODE_ELEMENT_NAME)) {
                    return (Node) nodeUnmarshaller.unmarshal(eventReader);
                }
            }

            eventReader.nextEvent();
        }

        return null;
    }

    @Override
    public void close() throws Exception {
        if (eventReader != null) {
            eventReader.close();
        }
    }
}
