package ru.nsu.fit.mazhuga.raspred.cmd.parsing;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import ru.nsu.fit.mazhuga.raspred.cmd.generated.Node;
import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders.DbNodeLoader;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class NodeParser {

    private static final String TAG_KEY_NAME = "name";

    public ParsingResult parseCompressed(String pathString) throws Exception {
        return parse(new BZip2CompressorInputStream(
                new BufferedInputStream(Files.newInputStream(Paths.get(pathString)))), null);
    }

    public ParsingResult parseCompressedAndLoadToDb(String pathString, DbNodeLoader dbLoader) throws Exception {
        return parse(new BZip2CompressorInputStream(
                new BufferedInputStream(Files.newInputStream(Paths.get(pathString)))), dbLoader);
    }

    private ParsingResult parse(InputStream inputStream, DbNodeLoader dbLoader)
            throws Exception {

        log.info("Node parsing started...");

        final var userEditsMap = new HashMap<String, Long>();
        final var nodeEditsMap = new HashMap<String, Long>();

        try (var reader = new NodeReader(inputStream)) {

            var node = reader.nextNode();

            while (node != null) {

                if (dbLoader != null) {
                    dbLoader.saveNode(node);
                }

                parseElementAndFillMaps(node, userEditsMap, nodeEditsMap);
                node = reader.nextNode();
            }
        }

        log.info("Node parsing finished!");

        Long dbInsertedCount = dbLoader != null ? dbLoader.getInsertedCount() : null;
        Long dbTimeSpentMs = dbLoader != null ? dbLoader.getTimeSpentMs() : null;

        return createResult(userEditsMap, nodeEditsMap, dbInsertedCount, dbTimeSpentMs);
    }

    private void parseElementAndFillMaps(Node node,
                                         Map<String, Long> userEditsMap,
                                         Map<String, Long> nodeEditsMap) {

        String userName = node.getUser();

        if (userEditsMap.containsKey(userName)) {
            Long editsValue = userEditsMap.get(userName);
            userEditsMap.put(userName, ++editsValue);
        } else {
            userEditsMap.put(userName, 1L);
        }

        log.trace("new edit for user {}, node id = {}", userName, node.getId());

        for (var tag : node.getTag()) {
            if (tag.getK().equals(TAG_KEY_NAME)) {
                String nodeName = tag.getV();

                if (nodeEditsMap.containsKey(nodeName)) {
                    Long editsValue = nodeEditsMap.get(nodeName);
                    nodeEditsMap.put(nodeName, ++editsValue);
                } else {
                    nodeEditsMap.put(nodeName, 1L);
                }

                log.trace("new occurrence for tag name {}, nodeId = {}", nodeName, node.getId());
            }
        }
    }

    private ParsingResult createResult(Map<String, Long> userEditsMap,
                                       Map<String, Long> nodeEditsMap,
                                       Long dbInsertedCount,
                                       Long dbTimeSpentMs) {

        final var userDataList = new ArrayList<ParsingResult.UserData>();
        final var nodeDataList = new ArrayList<ParsingResult.NodeData>();

        for (var mapEntry : userEditsMap.entrySet()) {
            var userData = new ParsingResult.UserData(mapEntry.getKey(), mapEntry.getValue());
            userDataList.add(userData);
        }

        for (var mapEntry : nodeEditsMap.entrySet()) {
            var nodeData = new ParsingResult.NodeData(mapEntry.getKey(), mapEntry.getValue());
            nodeDataList.add(nodeData);
        }

        return new ParsingResult(userDataList, nodeDataList, dbInsertedCount, dbTimeSpentMs);
    }
}
