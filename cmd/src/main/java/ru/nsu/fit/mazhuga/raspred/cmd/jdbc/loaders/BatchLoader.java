package ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders;

import org.apache.commons.lang3.time.StopWatch;
import ru.nsu.fit.mazhuga.raspred.cmd.generated.Node;

import java.sql.Connection;
import java.sql.SQLException;

public class BatchLoader extends PreparedStatementLoader {

    private final static int BATCH_SIZE = 1000;

    private int counter;

    protected BatchLoader(Connection connection) throws SQLException {
        super(connection);

        counter = 0;
    }

    @Override
    public void saveNode(Node node) throws SQLException {

        fillNodeStatement(node).addBatch();

        for (var tag : node.getTag()) {
            fillTagStatement(tag, node).addBatch();
        }

        counter += 1 + node.getTag().size();    // counts db entities: nodes and tags

        if (counter >= BATCH_SIZE) {
            executeBatch();
        }
    }

    private void executeBatch() throws SQLException {

        var stopWatch = StopWatch.createStarted();

        getNodeStatement().executeBatch();
        getTagStatement().executeBatch();

        getConnection().commit();

        stopWatch.stop();
        addInserted(counter);
        addTimeMs(stopWatch.getTime());

        counter = 0;
    }

    @Override
    public void close() throws SQLException {
        executeBatch();
        super.close();
    }
}
