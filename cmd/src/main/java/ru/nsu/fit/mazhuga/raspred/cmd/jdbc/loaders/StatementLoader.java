package ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders;

import org.apache.commons.lang3.time.StopWatch;
import ru.nsu.fit.mazhuga.raspred.cmd.generated.Node;

import java.sql.Connection;
import java.sql.SQLException;

public class StatementLoader extends AbstractNodeLoader {

    public StatementLoader(Connection connection) {
        super(connection);
    }

    @Override
    public void saveNode(Node node) throws SQLException {

        var stopWatch = StopWatch.createStarted();

        getConnection().createStatement().execute("insert into nodes" +
                "(id, lat, lon, usr, uid, visible, version, changeset, timestamp) " + "values (" +
                node.getId() + ", " +
                node.getLat() + ", " +
                node.getLon() + ", " +
                "'" + node.getUser().replace("'", "''") + "'" + ", " +
                node.getUid() + ", " +
                node.isVisible() + ", " +
                node.getVersion() + ", " +
                node.getChangeset() + ", " +
                "'" + node.getTimestamp() + "'" + ")");

        incrementInserted();

        for (var tag : node.getTag()) {

            getConnection().createStatement().execute("insert into tags(node_id, k, v) values(" +
                    node.getId() + ", " +
                    "'" + tag.getK().replace("'", "''") + "'" + ", " +
                    "'" + tag.getV().replace("'", "''") + "'" + ")");

            incrementInserted();
        }

        getConnection().commit();

        stopWatch.stop();
        addTimeMs(stopWatch.getTime());
    }

}
