package ru.nsu.fit.mazhuga.raspred.cmd.utils;

import lombok.Getter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Getter
public class PropertiesHolder {

    private static final String DB_URL_PROPERTY_NAME = "db.url";
    private static final String DB_USER_PROPERTY_NAME = "db.user";
    private static final String DB_PASSWORD_PROPERTY_NAME = "db.password";

    private final String databaseUrl;
    private final String databaseUser;
    private final String databasePassword;

    private PropertiesHolder(String databaseUrl, String databaseUser, String databasePassword) {
        this.databaseUrl = databaseUrl;
        this.databaseUser = databaseUser;
        this.databasePassword = databasePassword;
    }

    public static PropertiesHolder fromFile(String filePath) throws IOException {

        var properties = new Properties();

        try (InputStream in = PropertiesHolder.class.getResourceAsStream(filePath)) {
            properties.load(in);
        }

        return new PropertiesHolder(
                properties.getProperty(DB_URL_PROPERTY_NAME),
                properties.getProperty(DB_USER_PROPERTY_NAME),
                properties.getProperty(DB_PASSWORD_PROPERTY_NAME)
        );
    }
}
