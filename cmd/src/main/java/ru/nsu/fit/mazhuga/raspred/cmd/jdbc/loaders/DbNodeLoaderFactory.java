package ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders;

import ru.nsu.fit.mazhuga.raspred.cmd.jdbc.InsertionStrategy;

import java.sql.Connection;
import java.sql.SQLException;

public class DbNodeLoaderFactory {

    public DbNodeLoader create(InsertionStrategy strategy, Connection connection) throws SQLException {

        return switch (strategy) {
            case STATEMENT -> new StatementLoader(connection);
            case PREPARED_STATEMENT -> new PreparedStatementLoader(connection);
            case BATCH -> new BatchLoader(connection);
        };
    }
}
