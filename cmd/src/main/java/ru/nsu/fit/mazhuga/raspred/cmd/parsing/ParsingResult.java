package ru.nsu.fit.mazhuga.raspred.cmd.parsing;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ParsingResult {

    private List<UserData> userDataList;

    private List<NodeData> nodeDataList;

    private Long dbInsertedCount;

    private Long dbTimeSpentMs;

    @Data
    @AllArgsConstructor
    static class UserData {

        private String name;

        private long editsCount;
    }

    @Data
    @AllArgsConstructor
    static class NodeData {

        private String name;

        private long editsCount;
    }
}
