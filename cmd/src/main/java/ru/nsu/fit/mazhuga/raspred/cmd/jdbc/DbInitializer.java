package ru.nsu.fit.mazhuga.raspred.cmd.jdbc;

import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;

@Slf4j
public class DbInitializer {

    private static final String CREATE_NODES_TABLE_SQL =
            "create table if not exists nodes (" +
                    "id bigserial not null primary key, " +
                    "lat numeric(10, 7), " +
                    "lon numeric(10, 7), " +
                    "usr text, " +
                    "uid bigint, " +
                    "visible boolean, " +
                    "version bigint, " +
                    "changeset bigint, " +
                    "timestamp timestamp)";

    private static final String CREATE_TAGS_TABLE_SQL =
            "create table if not exists tags (" +
                    "node_id bigserial not null references nodes (id), " +
                    "k text, " +
                    "v text)";

    private static final String DROP_NODES_TABLE_SQL = "drop table if exists nodes";
    private static final String DROP_TAGS_TABLE_SQL = "drop table if exists tags";

    private final DbConnector dbConnector;

    public DbInitializer(DbConnector dbConnector) {
        this.dbConnector = dbConnector;
    }

    public void initDb(boolean clearData) {

        log.info("Database initialization started");

        try (final var connection = dbConnector.connect()) {

            if (clearData) {
                connection.prepareStatement(DROP_TAGS_TABLE_SQL).execute();
                connection.prepareStatement(DROP_NODES_TABLE_SQL).execute();
            }

            connection.prepareStatement(CREATE_NODES_TABLE_SQL).execute();
            connection.prepareStatement(CREATE_TAGS_TABLE_SQL).execute();

            connection.commit();

        } catch (SQLException e) {
            log.error("Could not create database tables", e);
        }

        log.info("Database initialized successfully!");
    }
}
