package ru.nsu.fit.mazhuga.raspred.cmd.jdbc;

import java.util.Arrays;

public enum InsertionStrategy {

    STATEMENT("statement"),
    PREPARED_STATEMENT("prepared"),
    BATCH("batch");

    private final String optionText;

    InsertionStrategy(String optionText) {
        this.optionText = optionText;
    }

    public static InsertionStrategy fromText(String optionText) {
        return Arrays.stream(InsertionStrategy.values())
                .filter(it -> it.optionText.equals(optionText))
                .findFirst()
                .orElse(null);
    }
}
