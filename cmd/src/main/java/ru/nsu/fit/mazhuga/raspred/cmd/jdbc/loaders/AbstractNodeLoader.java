package ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders;

import lombok.Getter;

import java.sql.Connection;
import java.sql.SQLException;

@Getter
public abstract class AbstractNodeLoader implements DbNodeLoader {

    private long insertedCount = 0;
    private long timeSpentMs = 0;

    private final Connection connection;

    protected AbstractNodeLoader(Connection connection) {
        this.connection = connection;
    }

    protected long addTimeMs(long toAdd) {
        timeSpentMs += toAdd;
        return timeSpentMs;
    }

    protected long addInserted(long toAdd) {
        insertedCount += toAdd;
        return insertedCount;
    }

    protected long incrementInserted() {
        return ++insertedCount;
    }

    @Override
    public void close() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

}
