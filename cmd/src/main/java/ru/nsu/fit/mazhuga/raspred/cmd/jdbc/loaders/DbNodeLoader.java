package ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders;

import ru.nsu.fit.mazhuga.raspred.cmd.generated.Node;

import java.sql.SQLException;

public interface DbNodeLoader extends AutoCloseable {

    void saveNode(Node node) throws SQLException;

    long getInsertedCount();

    long getTimeSpentMs();
}
