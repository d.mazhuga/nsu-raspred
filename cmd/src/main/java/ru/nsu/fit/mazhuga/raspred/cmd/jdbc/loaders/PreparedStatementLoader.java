package ru.nsu.fit.mazhuga.raspred.cmd.jdbc.loaders;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import ru.nsu.fit.mazhuga.raspred.cmd.generated.Node;
import ru.nsu.fit.mazhuga.raspred.cmd.generated.Tag;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Slf4j
public class PreparedStatementLoader extends AbstractNodeLoader {

    private final PreparedStatement nodeStatement;
    private final PreparedStatement tagStatement;

    public PreparedStatementLoader(Connection connection) throws SQLException {
        super(connection);

        nodeStatement = connection.prepareStatement("insert into nodes " +
                "(id, lat, lon, usr, uid, visible, version, changeset, timestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        tagStatement = connection.prepareStatement("insert into tags(node_id, k, v) values(?, ?, ?)");
    }

    @Override
    public void saveNode(Node node) throws SQLException {

        var stopWatch = StopWatch.createStarted();

        fillNodeStatement(node).execute();
        incrementInserted();

        for (var tag : node.getTag()) {
            fillTagStatement(tag, node).execute();
            incrementInserted();
        }

        getConnection().commit();

        stopWatch.stop();
        addTimeMs(stopWatch.getTime());

        if (getInsertedCount() % 1000 == 0) {
            log.info("inserted: " + getInsertedCount() + "; time: " + getTimeSpentMs());
        }
    }

    @Override
    public void close() throws SQLException {
        nodeStatement.close();
        tagStatement.close();
        super.close();
    }

    protected PreparedStatement getNodeStatement() {
        return nodeStatement;
    }

    protected PreparedStatement getTagStatement() {
        return tagStatement;
    }

    protected PreparedStatement fillNodeStatement(Node node) throws SQLException {

        int index = 1;

        nodeStatement.setLong(index++, node.getId().longValue());
        nodeStatement.setDouble(index++, node.getLat());
        nodeStatement.setDouble(index++, node.getLon());
        nodeStatement.setString(index++, node.getUser());
        nodeStatement.setLong(index++, node.getUid().longValue());
        nodeStatement.setObject(index++, node.isVisible());
        nodeStatement.setLong(index++, node.getVersion().longValue());
        nodeStatement.setLong(index++, node.getChangeset().longValue());
        nodeStatement.setObject(index, node.getTimestamp().toGregorianCalendar().toZonedDateTime().toLocalDateTime());

        return nodeStatement;
    }

    protected PreparedStatement fillTagStatement(Tag tag, Node node) throws SQLException {

        int index = 1;

        tagStatement.setLong(index++, node.getId().longValue());
        tagStatement.setString(index++, tag.getK());
        tagStatement.setString(index, tag.getV());

        return tagStatement;
    }

}
