package ru.nsu.fit.mazhuga.raspred.rest.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.mazhuga.raspred.rest.entity.Node;
import ru.nsu.fit.mazhuga.raspred.rest.service.NodeService;

import java.util.List;

@RestController
@RequestMapping("/node")
@AllArgsConstructor
public class NodeController {

    private final NodeService nodeService;

    @GetMapping("/{id}")
    public Node getById(@PathVariable Long id) {
        return nodeService.getById(id);
    }

    @GetMapping("/in-range")
    public List<Node> getInRange(@RequestParam Double latitude,
                                 @RequestParam Double longitude,
                                 @RequestParam Double range,
                                 @RequestParam(required = false, defaultValue = "10") Long limit) {
        return nodeService.getInRange(latitude, longitude, range, limit);
    }

    @PostMapping("/create")
    public Node create(@RequestBody Node node) {
        return nodeService.create(node);
    }

    @PutMapping("/update")
    public Node update(@RequestBody Node node) {
        return nodeService.update(node);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable Long id) {
        nodeService.deleteById(id);
    }

}
