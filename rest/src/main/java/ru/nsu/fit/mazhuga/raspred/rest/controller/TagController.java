package ru.nsu.fit.mazhuga.raspred.rest.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.mazhuga.raspred.rest.entity.Tag;
import ru.nsu.fit.mazhuga.raspred.rest.entity.TagId;
import ru.nsu.fit.mazhuga.raspred.rest.service.TagService;

@RestController
@RequestMapping("/tag")
@AllArgsConstructor
public class TagController {

    private final TagService tagService;

    @GetMapping("/get")
    public Tag getById(@RequestParam Long nodeId, @RequestParam String key) {
        return tagService.getById(new TagId(nodeId, key));
    }

    @PostMapping("/create")
    public Tag create(@RequestBody Tag tag) {
        return tagService.create(tag);
    }

    @PutMapping("/update")
    public Tag update(@RequestBody Tag tag) {
        return tagService.update(tag);
    }

    @DeleteMapping("/delete/")
    public void deleteById(@RequestParam Long nodeId, @RequestParam String key) {
        tagService.deleteById(new TagId(nodeId, key));
    }
}
