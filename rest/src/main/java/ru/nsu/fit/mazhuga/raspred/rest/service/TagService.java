package ru.nsu.fit.mazhuga.raspred.rest.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.fit.mazhuga.raspred.rest.entity.Node;
import ru.nsu.fit.mazhuga.raspred.rest.entity.Tag;
import ru.nsu.fit.mazhuga.raspred.rest.entity.TagId;
import ru.nsu.fit.mazhuga.raspred.rest.repository.TagRepository;

@Service
@AllArgsConstructor
public class TagService {

    private final TagRepository tagRepository;

    public Tag getById(TagId id) {
        return tagRepository.findById(id).orElseThrow();
    }

    public void deleteById(TagId id) {
        tagRepository.deleteById(id);
    }

    public Tag create(Tag tag) {

        if (tag.getId() != null && tagRepository.existsById(tag.getId())) {
            throw new IllegalArgumentException("Already exists");
        }

        return tagRepository.save(tag);
    }

    public Tag update(Tag tag) {

        if (tag.getId() == null || !tagRepository.existsById(tag.getId())) {
            throw new IllegalArgumentException("Does not exists");
        }

        return tagRepository.save(tag);
    }
}
