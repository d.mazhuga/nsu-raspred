package ru.nsu.fit.mazhuga.raspred.rest.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(TagId.class)
@Table(name = "tags")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Tag {

    @Id
    private Long nodeId;

    @Id
    private String k;

    private String v;

    public TagId getId() {

        if (nodeId == null || k == null) {
            return null;
        }

        return new TagId(nodeId, k);
    }
}
