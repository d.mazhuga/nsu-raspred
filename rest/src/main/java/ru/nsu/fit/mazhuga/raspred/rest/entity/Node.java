package ru.nsu.fit.mazhuga.raspred.rest.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "nodes")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Node {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double lat;

    private Double lon;

    private String usr;

    private Long uid;

    private Boolean visible;

    private Long version;

    private Long changeset;

    private LocalDateTime timestamp;

    @OneToMany(mappedBy = "nodeId")
    private List<Tag> tags = new ArrayList<>();
}
