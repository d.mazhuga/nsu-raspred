package ru.nsu.fit.mazhuga.raspred.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.fit.mazhuga.raspred.rest.entity.Node;

import java.util.List;

public interface NodeRepository extends JpaRepository<Node, Long> {

    @Query(
            value = "select * from nodes " +
                    "where earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(nodes.lat, nodes.lon)) < :rad " +
                    "order by earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(nodes.lat, nodes.lon))" +
                    "limit :lim",
            nativeQuery = true
    )
    List<Node> findInRange(
            @Param("lat") Double latitude,
            @Param("lon") Double longitude,
            @Param("rad") Double radius,
            @Param("lim") Long limit
    );

}
