package ru.nsu.fit.mazhuga.raspred.rest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagId implements Serializable {

    private Long nodeId;

    private String k;
}
