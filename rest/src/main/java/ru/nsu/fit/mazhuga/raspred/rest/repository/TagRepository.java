package ru.nsu.fit.mazhuga.raspred.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.mazhuga.raspred.rest.entity.Tag;
import ru.nsu.fit.mazhuga.raspred.rest.entity.TagId;

public interface TagRepository extends JpaRepository<Tag, TagId> {
}
