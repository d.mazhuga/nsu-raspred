package ru.nsu.fit.mazhuga.raspred.rest.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.fit.mazhuga.raspred.rest.entity.Node;
import ru.nsu.fit.mazhuga.raspred.rest.repository.NodeRepository;
import ru.nsu.fit.mazhuga.raspred.rest.repository.TagRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class NodeService {

    private final NodeRepository nodeRepository;

    private final TagRepository tagRepository;

    public Node getById(Long id) {
        return nodeRepository.findById(id).orElseThrow();
    }

    public List<Node> getInRange(double latitude, double longitude, double range, long limit) {
        return nodeRepository.findInRange(latitude, longitude, range, limit);
    }

    public void deleteById(Long id) {
        nodeRepository.deleteById(id);
    }

    public Node create(Node node) {

        if (node.getId() != null && nodeRepository.existsById(node.getId())) {
            throw new IllegalArgumentException("Already exists");
        }

        return save(node);
    }

    public Node update(Node node) {

        if (node.getId() == null || !nodeRepository.existsById(node.getId())) {
            throw new IllegalArgumentException("Does not exists");
        }

        return save(node);
    }

    private Node save(Node node) {

        var savedNode = nodeRepository.save(node);

        for (var tag : node.getTags()) {
            tag.setNodeId(savedNode.getId());
            tagRepository.save(tag);
        }

        return getById(savedNode.getId());
    }
}
